package com.devcamp.j01_javabasic.s50;

public class Voucher {
    public String voucherCode = "V000000";

    public String getVoucherCode(){
        return voucherCode;
    }
    public void setVoucherCode(String voucherCode){
        this.voucherCode = voucherCode;
    }
    public void showVoucher(){
        System.out.println("Voucher code is: " + this.voucherCode);
    }
    public void showVoucher(String voucherCode){
        System.out.println("This is voucher code: " + this.voucherCode);
        System.out.println("This is voucher code too: " + voucherCode);
    }
    public static void main(String[] args) throws Exception {
        Voucher voucher = new Voucher();
        //0.chay class nay roi comment dong code duoi
        //voucher.showVoucher();
        //1.bỏ comment dòng code dưới rồi chạy lại class này
       // voucher.showVoucher("CODE8888");
       //2.comment lại dòng code trên, bỏ comment 2 dòng code dưới rồi chạy lại class này
      // voucher.voucherCode = "CODE2222";
      // voucher.showVoucher("0008888");
      //3. comment 1 dòng code trên, bỏ comment 2 dòng code dưới rồi chạy lại code này
      String code = "VOUCHER";
        voucher.setVoucherCode(code);
        System.out.println(voucher.getVoucherCode());
        voucher.showVoucher(voucher.getVoucherCode());
        voucher.showVoucher();
    }
}
